#!/usr/bin/python

import threading
import RPi.GPIO as GPIO
import dht11
import time
import datetime
import sys 

class DHT11Sensor(threading.Thread):

    def __init__(self,gpioPin,period,DataToSend,id):
        threading.Thread.__init__(self)
        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BCM)
        self.sensor = dht11.DHT11(pin=gpioPin)
        self.period = period
        self.running = False
        self.queueForDataToSend = DataToSend
        self.id = id

    def run(self):
        self.running = True
        while self.running:
            result = self.sensor.read()
            if result.is_valid():
                #print("Temperature: %d C" % result.temperature)
                #print("Humidity: %d %%" % result.humidity)
                dataToSend = [str(datetime.datetime.now()),result.temperature,result.humidity,self.id]
                try:
                    self.queueForDataToSend.put_nowait(dataToSend)
                except Exception as inst:
                    print type(inst)   
                    print inst.args
                    print inst
                    print ("Queue full "+self.id)
                    sys.stdout.flush()
            else:
                while not result.is_valid():
                    result = self.sensor.read()
                dataToSend = [str(datetime.datetime.now()),result.temperature,result.humidity,self.id]
                try:
                    self.queueForDataToSend.put_nowait(dataToSend)
                except Exception as inst:
                    print type(inst)   
                    print inst.args
                    print inst
                    print ("Queue full "+self.id)
                    sys.stdout.flush()
            time.sleep(self.period)

    def stop(self):
        self.running = False