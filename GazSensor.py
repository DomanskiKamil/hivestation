#!/usr/bin/python

import threading
import Adafruit_ADS1x15
import time
import datetime
import sys
GAIN = 1
ADC_TO_VOLTS = 1.25*0.0001
ADC_CONVERSION_FACTOR = 1023.0/39999.0

class GazSensor(threading.Thread):

    def __init__(self,adcAddress,adcPins,mutex,period,DataToSend,id,Resistor=0.2):
        threading.Thread.__init__(self)
        self.adcDevice = Adafruit_ADS1x15.ADS1115(adcAddress)
        self.adcPins = adcPins
        self.mutex = mutex
        self.period = period
        self.running = False
        self.queueForDataToSend = DataToSend
        self.id = id
        self.resistor = Resistor

    def run(self):
        self.running = True
        self.GazLevelSum = 0
        self.GazLevelSumCount = 0
        self.currentTime = time.time()
        while self.running:
            GazLevel = self.calculateGazLevel(self.readAdc())
            self.GazLevelSum = self.GazLevelSum + GazLevel
            self.GazLevelSumCount = self.GazLevelSumCount + 1
            if(time.time()-self.currentTime > self.period):
                    self.currentTime = time.time()
                    AverageGazLevel = self.GazLevelSum/self.GazLevelSumCount
                    # print(str(self.id)+" "+str(AverageGazLevel))
                    dataToSend = [str(datetime.datetime.now()),AverageGazLevel,self.id]
                    try:
                        self.queueForDataToSend.put_nowait(dataToSend)
                    except Exception as inst:
                        print type(inst)   
                        print inst.args
                        print inst
                        print ("Queue full "+self.id)
                        sys.stdout.flush()
                    self.GazLevelSum = 0
                    self.GazLevelSumCount = 0
            

    def stop(self):
        self.running = False

    def readAdc(self):
        index = 0
        values = [0]*len(self.adcPins)
        self.mutex.acquire()
        for i in self.adcPins:
            values[index] = self.adcDevice.read_adc(i, gain=GAIN)
            index = index + 1
        self.mutex.release()
        return values

    def calculateGazLevel(self,values):
        return values[0]