import gspread
from oauth2client.service_account import ServiceAccountCredentials
import threading
import time
import datetime
import sys

class GoogleSheetSender(threading.Thread):

    def __init__(self, period, DataToSend):
        threading.Thread.__init__(self)
        self.running = False
        self.period = period
        self.queueForDataToSend = DataToSend

    def next_available_row(self, worksheet):
        str_list = filter(None, worksheet.col_values(1))  # fastest
        return len(str_list)+1

    def googleAthentication(self):
        try:
            self.scope = ['https://spreadsheets.google.com/feeds','https://www.googleapis.com/auth/drive']
            self.creds = ServiceAccountCredentials.from_json_keyfile_name('/home/pi/Programs/HiveStation-8c77d31f4762.json', self.scope)
            self.client = gspread.authorize(self.creds)
            self.sheet = self.client.open("HiveStationData")
        except Exception as inst:
            print type(inst)   
            print inst.args
            print inst
            sys.stdout.flush()
        

    def run(self):
        self.running = True
        self.googleAthentication()
        self.currentTime = time.time()
        i=1
        while self.running:
            if not self.queueForDataToSend.empty():
                try:
                    data = self.queueForDataToSend.get()
                    googleSheet = data[len(data)-1]
                    data = data[:len(data)-1]
                    # start = time.time()
                    worksheet = self.sheet.worksheet(googleSheet)
                    # print("Connection to sheet took: " + str(time.time() - start))
                    # start = time.time()
                    self.next_row = self.next_available_row(worksheet)
                    # print("Get nuber of rows took: " + str(time.time() - start))
                    # start = time.time()
                    worksheet.insert_row(data,self.next_row)
                    # print("Writing to sheet took: "  + str(time.time() - start))
                    # start = time.time()
                    #print(i)
                    i = i + 1
                except Exception as inst:
                    print type(inst)   
                    print inst.args
                    print inst
                    print(i)
                    sys.stdout.flush()
            else:
                time.sleep(self.period)
            if (time.time()-self.currentTime>120):
                self.googleAthentication()
                self.currentTime = time.time()

    def stop(self):
        self.running = False