#!/usr/bin/python

import threading
import WindSensorClass
import time
import LightSensor
import DHT11SensorClass
import RPi.GPIO as GPIO
import MicrophoneClass
# import GoogleSheetSender2
# import QIoTSender
import Queue
import MongoDBSender

DataToSend = Queue.Queue(100)
i2c_mutex1 = threading.Lock()
print("Start")
sensors = [LightSensor.LightSensor(id = "LightSensor",Address = 0x39, mutex = i2c_mutex1, period = 1, gain = 0, DataToSend = DataToSend),
            WindSensorClass.WindSensor(id ="WindSensor3", adcAddress = 0x49, adcPins = [2,3],mutex = i2c_mutex1,zeroWindAdjustment = 0.19,period = 1,DataToSend = DataToSend),
            WindSensorClass.WindSensor(id ="WindSensor2", adcAddress = 0x49, adcPins = [0,1],mutex = i2c_mutex1,zeroWindAdjustment = 0.195,period = 1,DataToSend = DataToSend),
            WindSensorClass.WindSensor(id ="WindSensor5", adcAddress = 0x4b, adcPins = [2,3],mutex = i2c_mutex1,zeroWindAdjustment = 0.205,period = 1,DataToSend = DataToSend),
            WindSensorClass.WindSensor(id ="WindSensor1", adcAddress = 0x48, adcPins = [1,0],mutex = i2c_mutex1,zeroWindAdjustment = 0.19,period = 1,DataToSend = DataToSend),
            WindSensorClass.WindSensor(id ="WindSensor4", adcAddress = 0x48, adcPins = [2,3],mutex = i2c_mutex1,zeroWindAdjustment = 0.19,period = 1,DataToSend = DataToSend),
            DHT11SensorClass.DHT11Sensor(id = "DHT11Out", gpioPin = 22, period = 5,DataToSend = DataToSend),
            DHT11SensorClass.DHT11Sensor(id = "DHT11In", gpioPin = 4, period = 5,DataToSend = DataToSend),
            MicrophoneClass.Microphone(id = "Microphone", period = 1, DataToSend = DataToSend)]

# dataSender = QIoTSender.QIoTSender(period = 1,DataToSend = DataToSend)
# dataSender = GoogleSheetSender2.GoogleSheetSender2(period = 20,DataToSend = DataToSend)  
dataSender = MongoDBSender.MongoDBSender(period = 1,DataToSend = DataToSend) 
for sensor in sensors:
    sensor.start()
dataSender.start()
try:
    while True:
        time.sleep(1)
        # DataToSend.queue.append(["50",50,"qiot/things/student/rpi/temp"])
        # DataToSend.queue.append([50,"qiot/things/student/rpi/test"])
except KeyboardInterrupt:
    for sensor in sensors:
        sensor.stop()
    dataSender.stop()
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)
    GPIO.cleanup()
