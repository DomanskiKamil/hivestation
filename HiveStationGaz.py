#!/usr/bin/python

import threading
import GazSensor
import time
import RPi.GPIO as GPIO
# import QIoTSender
import Queue
import MongoDBSender

DataToSend = Queue.Queue(100)
i2c_mutex1 = threading.Lock()
i2c_mutex2 = threading.Lock()
print("Start")
sensors = [GazSensor.GazSensor(id ="GazSensorMQ7", adcAddress = 0x49, adcPins = [1],mutex = i2c_mutex1, Resistor = 4.7, period = 1, DataToSend = DataToSend),
           GazSensor.GazSensor(id ="GazSensorMQ135", adcAddress = 0x49, adcPins = [0],mutex = i2c_mutex1, Resistor = 4.7, period = 1, DataToSend = DataToSend),
           GazSensor.GazSensor(id ="GazSensorTGS2603", adcAddress = 0x49, adcPins = [2],mutex = i2c_mutex1, Resistor = 4.7, period = 1, DataToSend = DataToSend)]

# dataSender = QIoTSender.QIoTSender(period = 1,DataToSend = DataToSend)
# dataSender = GoogleSheetSender2.GoogleSheetSender2(period = 20,DataToSend = DataToSend)  
dataSender = MongoDBSender.MongoDBSender(period = 1,DataToSend = DataToSend)
for sensor in sensors:
    sensor.start()
dataSender.start()
try:
    while True:
        time.sleep(1)
        # DataToSend.queue.append(["50",50,"qiot/things/student/rpi/temp"])
        # DataToSend.queue.append([50,"qiot/things/student/rpi/test"])
except KeyboardInterrupt:
    for sensor in sensors:
        sensor.stop()
    dataSender.stop()
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)
    GPIO.cleanup()
