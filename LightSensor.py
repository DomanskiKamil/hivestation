#!/usr/bin/python

import threading
import Adafruit_ADS1x15
import Adafruit_GPIO.I2C as I2C
import time
import datetime
import sys


class LightSensor(threading.Thread):
    #adcPins:
    # [0]: TEMP
    # [1]: RV
    def __init__(self,Address,mutex,period,gain,DataToSend,id):
        threading.Thread.__init__(self)
        self._device = I2C.get_i2c_device(Address)
        self.mutex = mutex
        self.period = period
        self.running = False
        self.queueForDataToSend = DataToSend
        self.id = id
        self.gain = gain

    def InitSensor(self):
        self.mutex.acquire()
        self._device.write8(0x80,0x01)
        self.mutex.release()
        time.sleep(2)
        self.mutex.acquire()
        self._device.write8(0x81,0x6C)
        self._device.write8(0x80,0x03)
        self._device.write8(0x82,0x1F)
        self._device.write8(0x87,self.gain)
        self.mutex.release()

    def run(self):
        self.InitSensor()
        self.running = True
        self.currentTime = time.time()
        self.luxSum = 0
        self.luxSumCount = 0
        while self.running:
            lux = self.CalculateLux(self.readAdc())
            # print(lux)
            # sys.stdout.flush()
            self.luxSum = self.luxSum + lux
            self.luxSumCount = self.luxSumCount + 1
            if(time.time()-self.currentTime > self.period):
                self.currentTime = time.time()
                Averagelux = self.luxSum/self.luxSumCount
                #print(str(self.id)+" "+str(AverageWindSpeed))
                dataToSend = [str(datetime.datetime.now()),Averagelux,self.id]
                try:
                    self.queueForDataToSend.put_nowait(dataToSend)
                except Exception as inst:
                    print type(inst)   
                    print inst.args
                    print inst
                    print ("Queue full "+self.id)
                    sys.stdout.flush()
                self.luxSum = 0
                self.luxSumCount = 0
            time.sleep(0.4)
            

    def stop(self):
        self.running = False

    def CalculateLux(self,adcVal):
        chScale0 = 65536
        if self.gain == 0:
            chScale1 = chScale0
        elif self.gain == 1:
            chScale0 = chScale0 >> 3
            chScale1 = chScale0
        elif self.gain == 2:
            chScale0 = chScale0 >> 4
            chScale1 = chScale0
        elif self.gain == 3:
            chScale1 = chScale0 / 115
            chScale0 = chScale0 / 107
        
        channel0 = (adcVal[0] * chScale0) >>  16
        channel1 = (adcVal[1] * chScale1) >>  16

        ratio1 = 0
        if channel0 != 0:
            ratio1 = (channel1 << (9 + 1)) / channel0

        ratio = (ratio1 + 1) >> 1

        if ((ratio >= 0) and (ratio <= 0x009A)):
            b = 0x2148
            m = 0x3d71
        elif ratio <= 0x00c3:
            b = 0x2a37
            m = 0x5b30
        elif ratio <= 0x00e6:
            b = 0x18ef
            m = 0x2db9
        elif ratio <= 0x0114:
            b = 0x0fdf
            m = 0x199a
        elif ratio > 0x0114:
            b = 0x0000
            m = 0x0000

        temp = ((channel0 * b) - (channel1 * m))
        temp = temp + 32768

        lux_temp = temp >> 16
        return lux_temp

    def readAdc(self):
        index = 0
        values = [0]*4
        adcVal = [0]*2
        self.mutex.acquire()
        for i in range(4):
            values[i] = self._device.readU8(0xD4+i)
        self.mutex.release()
        adcVal[0] = values[1]*256+values[0]
        adcVal[1] = values[3]*256+values[2]
        return adcVal

    def calculateWindSpeed(self,values):
        # print(values)
        RV_Wind_Volts = values[1]*ADC_TO_VOLTS
        TMP_Therm_ADunits = values[0]*ADC_CONVERSION_FACTOR
        TempCtimes100 = (0.005 *(TMP_Therm_ADunits * TMP_Therm_ADunits)) - (16.862 * TMP_Therm_ADunits) + 9075.4

        zeroWind_ADunits = -0.0006*(TMP_Therm_ADunits * TMP_Therm_ADunits) + 1.0727 * TMP_Therm_ADunits + 47.172
        zeroWind_volts = (zeroWind_ADunits * 0.0048828125) - self.zeroWindAdjustment 
        # print zeroWind_volts
        # print zeroWind_ADunits
        try:
            WindSpeed_KMPH = pow(((RV_Wind_Volts - zeroWind_volts) /.2300) , 2.7265)*1.609
        except:
            WindSpeed_KMPH = 0
        # print WindSpeed_KMPH
        return WindSpeed_KMPH