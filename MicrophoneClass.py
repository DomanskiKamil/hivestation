import threading
import pyaudio
import sys
from StringIO import StringIO
from pydub import AudioSegment
import datetime
import time

FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 22050
CHUNK = 4096
DEVICE_INDEX = 4

class Microphone(threading.Thread):

    def __init__(self, period, DataToSend, id):
        threading.Thread.__init__(self)
        self.running = False
        self.period = period
        self.queueForDataToSend = DataToSend
        self.id = id
        self.currentTime = time.time()

    def stop(self):
        self.running = False
        try:
            self.stream.stop_stream()
            self.stream.close()
        except Exception as inst:
            print type(inst)   
            print inst.args
            print inst
            print("AudioStreamError "+self.id)
            sys.stdout.flush()

    def openStream(self):
        try:
            self.running = True
            self.audio = pyaudio.PyAudio()
            self.stream = self.audio.open(format=FORMAT,
                    channels=CHANNELS,
                    rate=RATE,
                    input=True,
                    frames_per_buffer=CHUNK,
                    input_device_index=DEVICE_INDEX,
                    start=True)
        except Exception as inst:
            print type(inst)   
            print inst.args
            print inst
            sys.stdout.flush()
                        
    def closeStream(self):
        try:
            self.stream.stop_stream()
            self.stream.close()
        except Exception as inst:
            print type(inst)   
            print inst.args
            print inst
            
    def putDataInQueue(self,dataToSend):
        try:
            self.queueForDataToSend.put_nowait(dataToSend)
        except Exception as inst:
            print type(inst)   
            print inst.args
            print inst
            print ("Queue full "+self.id)
            sys.stdout.flush()
            
    def run(self):
        print("MicStarted")
        self.rmsSum = 0
        self.rmsSumCount = 0
        self.openStream()
        while self.running:
            try:
                data = self.stream.read(CHUNK)
                dataString = StringIO(data)
                raw_audio = AudioSegment.from_file(dataString, format="raw",
                               frame_rate=RATE, channels=CHANNELS, sample_width=2)
                after_offset = raw_audio.remove_dc_offset()
                louder = after_offset + 12
                louder.export(dataString, format="raw")
                
                self.rmsSum = self.rmsSum + louder.rms
                self.rmsSumCount = self.rmsSumCount + 1
                if(time.time()-self.currentTime > self.period):
                    self.currentTime = time.time()
                    #print(str(self.id)+" "+str(self.rmsSum/self.rmsSumCount))
                    dataToSend = [str(datetime.datetime.now()),self.rmsSum/self.rmsSumCount,self.id]
                    self.putDataInQueue(dataToSend)    
                    self.rmsSum = 0
                    self.rmsSumCount = 0
                        
            except Exception as inst:
                print type(inst)   
                print inst.args
                print inst
                sys.stdout.flush()
                self.closeStream()
                self.openStream()
                
                



