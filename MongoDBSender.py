from lib import qiot
import pymongo
import random
import threading
import time
import datetime
import sys
MONGO_ADDRESS = "mongodb://qiotuser:qiotpass@172.20.83.35:27017/HiveStation"

class MongoDBSender(threading.Thread):

    def __init__(self, period, DataToSend):
        threading.Thread.__init__(self)
        self.running = False
        self.period = period
        self.queueForDataToSend = DataToSend
        self.database = None
        self.clientConnected = False

    def Initconnection(self):
        con = pymongo.MongoClient(MONGO_ADDRESS)
        self.database = con['HiveStation']

    def run(self):
        self.running = True
        self.Initconnection()
        print("Start mongoConnect")
        while self.running:
            if not self.queueForDataToSend.empty():
                try:
                    data = self.queueForDataToSend.get()
                    topicId = data[len(data)-1]
                    times = data[0]
                    data = data[1:len(data)-1]
                    # print(topicId)
                    collection = self.database[topicId]
                    if topicId == "RFID":
                        d = {"value": [0], "UID":str(data), "timestamp": times}    
                    else:
                        d = {"value": str(data), "timestamp": times}
                    collection.insert_one(d)
                except Exception as inst:
                    print type(inst)   
                    print inst.args
                    print inst
                    sys.stdout.flush()
                    # time.sleep(1)
                    self.Initconnection()
            else:
                time.sleep(self.period)
                

    def stop(self):
        self.running = False