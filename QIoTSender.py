from lib import qiot
import random
import threading
import time
import datetime
import sys

class QIoTSender(threading.Thread):

    def __init__(self, period, DataToSend):
        threading.Thread.__init__(self)
        self.running = False
        self.period = period
        self.queueForDataToSend = DataToSend
        self.connection = None
        self.clientConnected = False

    def onConnect(self,event_trigger,data):
        print "client ready"
        self.clientConnected = True

    def Initconnection(self):
        self.connection = qiot.connection(qiot.protocol.MQTT)
        self.connectionOptions = self.connection.read_resource('/home/pi/Programs/res/resourceinfo.json','./ssl/')
        self.connection.on("connect",self.onConnect)
        self.connection.connect(self.connectionOptions)

    def run(self):
        self.running = True
        self.Initconnection()

        while self.running:
            if self.clientConnected:
                if not self.queueForDataToSend.empty():
                    try:
                        data = self.queueForDataToSend.get()
                        topicId = data[len(data)-1]
                        times = data[0]
                        data = data[1:len(data)-1]
                        # print(topicId)
                        if topicId == "RFID":
                            self.connection.publish_by_topic("qiot/things/student/rpi/"+topicId,  "{\"value\":" + "[0]"+ ",\"UID\":\""+ str(data) +"\",\"timestamp\":\""+ times +"\"}")
                        else:
                            self.connection.publish_by_topic("qiot/things/student/rpi/"+topicId,  "{\"value\":" + str(data) + ",\"timestamp\":\""+ times +"\"}")
                    except Exception as inst:
                        print type(inst)   
                        print inst.args
                        print inst
                        sys.stdout.flush()
                        time.sleep(1)
                else:
                    time.sleep(self.period)
                

    def stop(self):
        self.running = False