import gspread
from oauth2client.service_account import ServiceAccountCredentials
import threading
import time
import datetime
import sys

class GoogleSheetSender2(threading.Thread):

    def __init__(self, period, DataToSend):
        threading.Thread.__init__(self)
        self.running = False
        self.period = period
        self.queueForDataToSend = DataToSend

    def next_available_row(self, worksheet):
        str_list = filter(None, worksheet.col_values(1))  # fastest
        return len(str_list)+1

    def googleAthentication(self):
        try:
            self.scope = ['https://spreadsheets.google.com/feeds','https://www.googleapis.com/auth/drive']
            self.creds = ServiceAccountCredentials.from_json_keyfile_name('/home/pi/Programs/HiveStation-8c77d31f4762.json', self.scope)
            self.client = gspread.authorize(self.creds)
            self.sheet = self.client.open("HiveStationRFID")
        except Exception as inst:
            print type(inst)   
            print inst.args
            print inst
            sys.stdout.flush()
        

    def run(self):
        self.running = True
        self.googleAthentication()
        self.currentTime = time.time()
        self.Sheets = {}
        i=1
        while self.running:
            if not self.queueForDataToSend.empty():
                try:
                    data = self.queueForDataToSend.get()
                    googleSheet = data[len(data)-1]
                    data = data[:len(data)-1]
                    # start = time.time()
                    if(self.Sheets.has_key(googleSheet)):
                        worksheet = self.Sheets[googleSheet]
                    else:    
                        worksheet = self.sheet.worksheet(googleSheet)
                        self.Sheets[googleSheet] = worksheet
                        self.Sheets[googleSheet+"RowNumber"] = self.next_available_row(worksheet)
                    # print("Connection to sheet took: " + str(time.time() - start))
                    # start = time.time()
                    # self.next_row = self.next_available_row(worksheet)
                    # print("Get nuber of rows took: " + str(time.time() - start))
                    # start = time.time()
                    worksheet.insert_row(data,self.Sheets[googleSheet+"RowNumber"])
                    # print("Writing to sheet took: "  + str(time.time() - start))
                    # start = time.time()
                    #print(i)
                    self.Sheets[googleSheet+"RowNumber"] = self.Sheets[googleSheet+"RowNumber"] + 1
                    i = i + 1
                except Exception as inst:
                    print type(inst)   
                    print inst.args
                    print inst
                    print(i)
                    sys.stdout.flush()
                    time.sleep(self.period)
            else:
                time.sleep(self.period)
            if (time.time()-self.currentTime>120):
                self.googleAthentication()
                self.currentTime = time.time()
                self.Sheets = {}

    def stop(self):
        self.running = False