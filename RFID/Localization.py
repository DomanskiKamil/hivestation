#!/usr/bin/python

import threading
import time
import RPi.GPIO as GPIO
import GoogleSheetSender2

import RFIDReaderClass
import Queue

import sys
sys.path.insert(0, '/home/pi/Programs')
import QIoTSender
DataToSend = Queue.Queue(100)
spi_mutex1 = threading.Lock()
print("Start")
sensors = [RFIDReaderClass.RFIDReader(id = "RFID", DataToSend = DataToSend,CSPin=1,IRQPin=1)]

# dataSender = GoogleSheetSender2.GoogleSheetSender2(period = 20,DataToSend = DataToSend) 
dataSender = QIoTSender.QIoTSender(period = 1,DataToSend = DataToSend)
for sensor in sensors:
    sensor.start()
dataSender.start()
try:
    while True:
        time.sleep(1)
except KeyboardInterrupt:
    for sensor in sensors:
        sensor.stop()
    dataSender.stop()
    GPIO.setwarnings(False)
    GPIO.cleanup()
