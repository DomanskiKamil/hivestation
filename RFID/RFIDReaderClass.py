#!/usr/bin/python

import threading
import RPi.GPIO as GPIO
from pirc522 import RFID
import time
import datetime
import sys 

class RFIDReader(threading.Thread):

    def __init__(self,id,DataToSend,CSPin,IRQPin):
        threading.Thread.__init__(self)
        GPIO.setwarnings(False)
        
        self.sensor = RFID()
        self.sensorUtil = self.sensor.util()
        self.sensorUtil.debug = True
        self.running = [False]
        self.queueForDataToSend = DataToSend
        self.sendingQueue = []
        self.id = id


    def run(self):
        self.running[0] = [True]
        while self.running[0]:
            self.sensor.wait_for_tag(self.running)
            dataToSend = [str(datetime.datetime.now())]
            print("IRQ")
            (error, data) = self.sensor.request()
            if not error:
                print("\nDetected: " + format(data, "02x"))

            (error, uid) = self.sensor.anticoll()
            if not error:
                print("Card read UID: "+str(uid[0])+","+str(uid[1])+","+str(uid[2])+","+str(uid[3]))
                dataToSend.append(str(uid[0])+","+str(uid[1])+","+str(uid[2])+","+str(uid[3]))
            if len(dataToSend) == 1:
                dataToSend.append("")    
            dataToSend.append(self.id)
            self.sendingQueue.append(dataToSend)
            SendingData = []
            try:
                while len(self.sendingQueue) > 0:
                    SendingData = self.sendingQueue.pop(0)
                    self.queueForDataToSend.put_nowait(SendingData)

            except Exception as inst:
                print type(inst)   
                print inst.args
                print inst
                print ("Queue full "+self.id)
                self.sendingQueue.append(SendingData)
                sys.stdout.flush()
            time.sleep(1)
        self.sensor.cleanup()

    def stop(self):
        global run
        print("\n"+self.id+" Stoped\n")
        self.running[0] = False
        