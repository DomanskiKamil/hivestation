#!/usr/bin/python

import threading
import WindSensorClass
import time
import DHT11SensorClass
import RPi.GPIO as GPIO
import MicrophoneClass
import GoogleSheetSender
import Queue

DataToSend = Queue.Queue(50)
i2c_mutex1 = threading.Lock()
i2c_mutex2 = threading.Lock()
print("Start")
sensors = [WindSensorClass.WindSensor(adcAddress = 0x48, adcPins = [3,2],mutex = i2c_mutex1,zeroWindAdjustment = 0.2,period = 5,DataToSend = DataToSend,googleSheet ="WindSensor"),
            WindSensorClass.WindSensor(adcAddress = 0x49, adcPins = [0,1],mutex = i2c_mutex1,zeroWindAdjustment = 0.19,period = 5,DataToSend = DataToSend,googleSheet ="WindSensor-CzarnaTasma")]

# dataSender = GoogleSheetSender.GoogleSheetSender(period = 5,DataToSend = DataToSend) 
for sensor in sensors:
    sensor.start()
# dataSender.start()
try:
    while True:
        time.sleep(0.1)
        DataToSend.get()
except KeyboardInterrupt:
    for sensor in sensors:
        sensor.stop()
    # dataSender.stop()
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)
    GPIO.cleanup()
