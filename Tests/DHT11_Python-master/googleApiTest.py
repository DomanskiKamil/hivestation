import gspread
from oauth2client.service_account import ServiceAccountCredentials
import datetime
import time

import RPi.GPIO as GPIO
import dht11

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.cleanup()

instance = dht11.DHT11(pin=4)

def next_available_row(worksheet):
    str_list = filter(None, worksheet.col_values(1))  # fastest
    return len(str_list)+1

# use creds to create a client to interact with the Google Drive API
scope = ['https://spreadsheets.google.com/feeds','https://www.googleapis.com/auth/drive']
creds = ServiceAccountCredentials.from_json_keyfile_name('/home/pi/Programs/Tests/DHT11_Python-master/HiveStation-8c77d31f4762.json', scope)
client = gspread.authorize(creds)

# Find a workbook by name and open the first sheet
# Make sure you use the right name here.
sheet = client.open("HiveStationData").sheet1

# Extract and print all of the values
# list_of_hashes = sheet.get_all_records()
# print(list_of_hashes)
next_row = next_available_row(sheet)
i=0
row = [0]*3
currentTime = time.time()
try:
    print("Start")
    while True:
        result = instance.read()
        if result.is_valid():
            print("valid data")
            row[0] = str(datetime.datetime.now())
            row[1] =  result.temperature
            row[2] =  result.humidity
            sheet.insert_row(row,next_row)
            i=i+1
            next_row = int(next_row) + 1

        time.sleep(5)
	if (time.time()-currentTime>120):
		creds = ServiceAccountCredentials.from_json_keyfile_name('/home/pi/Programs/Tests/DHT11_Python-master/HiveStation-8c77d31f4762.json', scope)
		client = gspread.authorize(creds)
		sheet = client.open("HiveStationData").sheet1
		next_row = next_available_row(sheet)
		currentTime = time.time()
except KeyboardInterrupt:
    pass
