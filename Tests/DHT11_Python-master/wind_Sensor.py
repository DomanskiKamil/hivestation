import time
import datetime
import Adafruit_ADS1x15
import RPi.GPIO as GPIO
import dht11

# initialize GPIO
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.cleanup()

# read data using pin 14
dht11 = dht11.DHT11(pin=4)
#TMP A0
#RV A1

Sensor = Adafruit_ADS1x15.ADS1115(0x48)

GAIN = 1
zeroWindAdjustment = 0.3
ADC_TO_VOLTS = 1.25*0.0001
try:
    while True:
        values = [0]*2
        for i in range(2):
            # Read the specified ADC channel using the previously set gain value.
            values[i] = Sensor.read_adc(i, gain=GAIN)
            #values[i] = values[i]*ADC_TO_VOLTS
        print(str(values[0]*ADC_TO_VOLTS)+"    "+str(values[1]*ADC_TO_VOLTS))
        RV_Wind_Volts = values[1]*ADC_TO_VOLTS
        TMP_Therm_ADunits = values[0]*1023.0/39999.0
        print(str(TMP_Therm_ADunits))
        TempCtimes100 = (0.005 *(TMP_Therm_ADunits * TMP_Therm_ADunits)) - (16.862 * TMP_Therm_ADunits) + 9075.4

        zeroWind_ADunits = -0.0006*(TMP_Therm_ADunits * TMP_Therm_ADunits) + 1.0727 * TMP_Therm_ADunits + 47.172
        zeroWind_volts = (zeroWind_ADunits * 0.0048828125) - zeroWindAdjustment 
        print(str(zeroWind_volts)+"    "+str(RV_Wind_Volts))
        try:
            WindSpeed_MPH = pow(((RV_Wind_Volts - zeroWind_volts) /.2300) , 2.7265)#*1.609*(10.0/36.0)*10**10; 
        except:
            WindSpeed_MPH = 0
        file = open('wind_stamp.csv','a')
        file.write(str(datetime.datetime.now())+',')
        file.write(str(values[0])+',')
        file.write(str(values[1])+',')
        file.write(str(WindSpeed_MPH)+',')
        file.write(str(TempCtimes100)+',')

        result = dht11.read()
        while(not result.is_valid()):
            result = dht11.read()
        if result.is_valid():
            file.write(str(result.temperature)+',')
            file.write(str(result.humidity)+'\n')
            print('| {0:>10} | {1:>10} |'.format(*values)+str(WindSpeed_MPH)+" | "+str(TempCtimes100)+" |   "+ str(result.temperature)+"|"+ str(result.humidity))
        file.close()
        time.sleep(1)
except KeyboardInterrupt:
    pass


