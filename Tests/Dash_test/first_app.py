# -*- coding: utf-8 -*-
import dash
import dash_core_components as dcc
import dash_html_components as html
import gspread
from oauth2client.service_account import ServiceAccountCredentials
import pandas as pd
import plotly.plotly as py
import plotly.graph_objs as go
import sys
def googleAthentication():
    try:
        scope = ['https://spreadsheets.google.com/feeds','https://www.googleapis.com/auth/drive']
        creds = ServiceAccountCredentials.from_json_keyfile_name('/home/pi/Programs/HiveStation-8c77d31f4762.json', scope)
        client = gspread.authorize(creds)
        sheet = client.open("HiveStationData")
        return sheet
    except Exception as inst:
        print type(inst)   
        print inst.args
        print inst
        sys.stdout.flush()
    return None

app = dash.Dash()

app.layout = html.Div(children=[
    html.H1(children='Hive data'),
    html.Button('Update', id='button'),
    dcc.Graph(id='DHT112'),
    dcc.Graph(id='WindSensors'),
    dcc.Graph(id='Microphon')  
])
# @app.callback(
#     dash.dependencies.Output('DHT112', 'figure'),
#     [dash.dependencies.Input('button', 'n_clicks')])
# def update_DHT_figure(n_clicks):
#     sheet = googleAthentication()
#     worksheet = sheet.worksheet("DHT11")
#     dataframe = pd.DataFrame(worksheet.get_all_records(empty2zero=True))
#     trace_high = go.Scatter(
#         x=dataframe['Data'],
#         y=dataframe['Temperatura'],
#         name = "Temperatura",
#         line = dict(color = '#17BECF'),
#         opacity = 0.8)

#     trace_low = go.Scatter(
#         x=dataframe['Data'],
#         y=dataframe['Wilgotnosc'],
#         name = "Wilgotnosc",
#         line = dict(color = '#7F7F7F'),
#         opacity = 0.8)

#     data = [trace_high,trace_low]

#     layout = dict(
#         title='Wilgotność i temperatura',
#         xaxis=dict(
#             rangeselector=dict(
#                 buttons=list([
#                     dict(count=1,
#                         label='1h',
#                         step='houre',
#                         stepmode='backward'),
#                     dict(count=1,
#                         label='1d',
#                         step='day',
#                         stepmode='backward'),
#                     dict(step='all')
#                 ])
#             ),
#             rangeslider=dict(
#                 visible = True
#             ),
#             type='date'
#         )
#     )
#     return dict(data=data, layout=layout)

@app.callback(
dash.dependencies.Output('WindSensors', 'figure'),
    [dash.dependencies.Input('button', 'n_clicks')])
def update_WindSensor_figure(n_clicks):
    sheet = googleAthentication()
    worksheet = sheet.worksheet("WindSensor")
    dataframe = pd.DataFrame(worksheet.get_all_records())
    worksheet = sheet.worksheet("WindSensor-CzarnaTasma")
    dataframe2 = pd.DataFrame(worksheet.get_all_records())
    trace_high = go.Scatter(
            x=dataframe['Data'],
            y=dataframe['windSpeed'],
            name = "Wind speed1",
            line = dict(color = '#17BECF'),
            opacity = 0.8)

    trace_low = go.Scatter(
        x=dataframe2['Data'],
        y=dataframe2['windSpeed'],
        name = "Wind speed1",
        line = dict(color = '#7F7F7F'),
        opacity = 0.8)

    data = [trace_high,trace_low]

    layout = dict(
        title='Prędkość wiatru',
        xaxis=dict(
            rangeselector=dict(
                buttons=list([
                    dict(count=1,
                        label='1h',
                        step='houre',
                        stepmode='backward'),
                    dict(count=1,
                        label='1d',
                        step='day',
                        stepmode='backward'),
                    dict(step='all')
                ])
            ),
            rangeslider=dict(
                visible = True
            ),
            type='date'
        )
    )
    return dict(data=data, layout=layout)

@app.callback(
dash.dependencies.Output('Microphon', 'figure'),
    [dash.dependencies.Input('button', 'n_clicks')])
def update_Microphon_figure(n_clicks):    

    sheet = googleAthentication()
    worksheet = sheet.worksheet("Microphone")
    dataframe = pd.DataFrame(worksheet.get_all_records())

    trace_high = go.Scatter(
            x=dataframe['Data'],
            y=dataframe['RMS'],
            name = "RMS",
            line = dict(color = '#17BECF'),
            opacity = 0.8)

    data = [trace_high]

    layout = dict(
        title='Natężenie dźwięku',
        xaxis=dict(
            rangeselector=dict(
                buttons=list([
                    dict(count=1,
                        label='1h',
                        step='houre',
                        stepmode='backward'),
                    dict(count=1,
                        label='1d',
                        step='day',
                        stepmode='backward'),
                    dict(step='all')
                ])
            ),
            rangeslider=dict(
                visible = True
            ),
            type='date'
        )
    )
    return dict(data=data, layout=layout)


if __name__ == '__main__':
    app.run_server(debug=True, host='0.0.0.0',port=80)#,ssl_context='adhoc')