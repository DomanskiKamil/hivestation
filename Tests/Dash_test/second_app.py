# -*- coding: utf-8 -*-
import dash
import dash_core_components as dcc
import dash_html_components as html
# import gspread
# from oauth2client.service_account import ServiceAccountCredentials
# import pandas as pd
import plotly.plotly as py
import plotly.graph_objs as go
import sys
import pymongo
import datetime
app = dash.Dash()

app.layout = html.Div(children=[
    html.H1(children='Hive data'),
    html.Button('Update', id='button'),
    dcc.Graph(id='DHT11'),
    dcc.Graph(id='WindSensors'),
    dcc.Graph(id='Microphon'),
    dcc.Graph(id='Gaz_Sensors')  

])
@app.callback(
    dash.dependencies.Output('DHT11', 'figure'),
    [dash.dependencies.Input('button', 'n_clicks')])
def update_DHT_figure(n_clicks):
    connection = pymongo.MongoClient("mongodb://192.168.1.6:27017")
    database = connection['HiveStation']
    collection = database["DHT11In"]
    point = collection.find()
    time = []
    data = []
    data2 = []
    for x in point:
        time.append(x["timestamp"])
        data.append(x["value"][0])
        data2.append(x["value"][1])
    trace_high = go.Scatter(
        x=time,
        y=data,
        name = "Temperatura",
        line = dict(color = '#17BECF'),
        opacity = 0.8)

    trace_low = go.Scatter(
        x=time,
        y=data2,
        name = "Wilgotnosc",
        line = dict(color = '#7F7F7F'),
        opacity = 0.8)

    data = [trace_high,trace_low]

    layout = dict(
        title='Wilgotność i temperatura',
        xaxis=dict(
            rangeselector=dict(
                buttons=list([
                    dict(count=1,
                        label='1h',
                        step='houre',
                        stepmode='backward'),
                    dict(count=1,
                        label='1d',
                        step='day',
                        stepmode='backward'),
                    dict(step='all')
                ])
            ),
            rangeslider=dict(
                visible = True
            ),
            type='date'
        )
    )
    return dict(data=data, layout=layout)

@app.callback(
dash.dependencies.Output('WindSensors', 'figure'),
    [dash.dependencies.Input('button', 'n_clicks')])
def update_WindSensor_figure(n_clicks):
    connection = pymongo.MongoClient("mongodb://192.168.1.6:27017")
    database = connection['HiveStation']
    collection = database["WindSensor2"]
    point = collection.find()
    collection = database["WindSensor3"]
    point2 = collection.find()
    collection = database["WindSensor5"]
    point3 = collection.find()
    time = []
    data = []
    for x in point:
        time.append(x["timestamp"])
        data.append(x["value"][0])
    trace_high = go.Scatter(
        x=time,
        y=data,
        name = "Wind speed2",
        line = dict(color = '#17BECF'),
        opacity = 0.8)
    for x in point2:
        time.append(x["timestamp"])
        data.append(x["value"][0])
    trace_low = go.Scatter(
        x=time,
        y=data,
        name = "Wind speed3",
        line = dict(color = '#7F7F7F'),
        opacity = 0.8)
    for x in point3:
        time.append(x["timestamp"])
        data.append(x["value"][0])
    trace_mid = go.Scatter(
        x=time,
        y=data,
        name = "Wind speed5",
        line = dict(color = '#7F7F7F'),
        opacity = 0.8)
    data = [trace_high,trace_low,trace_mid]

    layout = dict(
        title='Prędkość wiatru',
        xaxis=dict(
            rangeselector=dict(
                buttons=list([
                    dict(count=1,
                        label='1h',
                        step='houre',
                        stepmode='backward'),
                    dict(count=1,
                        label='1d',
                        step='day',
                        stepmode='backward'),
                    dict(step='all')
                ])
            ),
            rangeslider=dict(
                visible = True
            ),
            type='date'
        )
    )
    return dict(data=data, layout=layout)

@app.callback(
dash.dependencies.Output('Gaz_Sensors', 'figure'),
    [dash.dependencies.Input('button', 'n_clicks')])
def update_GazSensors_figure(n_clicks):
    connection = pymongo.MongoClient("mongodb://192.168.1.6:27017")
    database = connection['HiveStation']
    collection = database["GazSensorMQ135"]
    point = collection.find()
    collection = database["GazSensorMQ7"]
    point2 = collection.find()
    collection = database["GazSensorTGS2603"]
    point3 = collection.find()
    time = []
    data = []
    for x in point:
        time.append(x["timestamp"])
        data.append(x["value"][0])
    trace_high = go.Scatter(
        x=time,
        y=data,
        name = "GazSensorMQ135",
        line = dict(color = '#17BECF'),
        opacity = 0.8)
    for x in point2:
        time.append(x["timestamp"])
        data.append(x["value"][0])
    trace_low = go.Scatter(
        x=time,
        y=data,
        name = "GazSensorMQ7",
        line = dict(color = '#7F7F7F'),
        opacity = 0.8)
    for x in point3:
        time.append(x["timestamp"])
        data.append(x["value"][0])
    trace_mid = go.Scatter(
        x=time,
        y=data,
        name = "GazSensorTGS2603",
        line = dict(color = '#FF0000'),
        opacity = 0.8)
    data = [trace_high,trace_low,trace_mid]

    layout = dict(
        title='Czujniki gazu',
        xaxis=dict(
            rangeselector=dict(
                buttons=list([
                    dict(count=1,
                        label='1h',
                        step='houre',
                        stepmode='backward'),
                    dict(count=1,
                        label='1d',
                        step='day',
                        stepmode='backward'),
                    dict(step='all')
                ])
            ),
            rangeslider=dict(
                visible = True
            ),
            type='date'
        )
    )
    return dict(data=data, layout=layout)


@app.callback(
dash.dependencies.Output('Microphon', 'figure'),
    [dash.dependencies.Input('button', 'n_clicks')])
def update_Microphon_figure(n_clicks):    

    connection = pymongo.MongoClient("mongodb://192.168.1.6:27017")
    database = connection['HiveStation']
    collection = database["Microphone"]
    point = collection.find()
    time = []
    data = []
    for x in point:
        time.append(x["timestamp"])
        data.append(x["value"][0])

    trace_high = go.Scatter(
            x=time,
            y=data,
            name = "RMS",
            line = dict(color = '#17BECF'),
            opacity = 0.8)

    data = [trace_high]

    layout = dict(
        title='Natężenie dźwięku',
        xaxis=dict(
            rangeselector=dict(
                buttons=list([
                    dict(count=1,
                        label='1h',
                        step='houre',
                        stepmode='backward'),
                    dict(count=1,
                        label='1d',
                        step='day',
                        stepmode='backward'),
                    dict(step='all')
                ])
            ),
            rangeslider=dict(
                visible = True
            ),
            type='date'
        )
    )
    return dict(data=data, layout=layout)


if __name__ == '__main__':
    app.run_server(debug=True, host='0.0.0.0',port=80)#,ssl_context='adhoc')