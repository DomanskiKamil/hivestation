#!/usr/bin/python

import threading
import Adafruit_ADS1x15
import time
import datetime
import sys
GAIN = 1
ADC_TO_VOLTS = 1.25*0.0001
ADC_CONVERSION_FACTOR = 1023.0/39999.0

class WindSensor(threading.Thread):
    #adcPins:
    # [0]: TEMP
    # [1]: RV
    def __init__(self,adcAddress,adcPins,mutex,period,DataToSend,id,zeroWindAdjustment=0.2):
        threading.Thread.__init__(self)
        self.adcDevice = Adafruit_ADS1x15.ADS1115(adcAddress)
        self.adcPins = adcPins
        self.mutex = mutex
        self.period = period
        self.running = False
        self.queueForDataToSend = DataToSend
        self.id = id
        self.zeroWindAdjustment = zeroWindAdjustment

    def run(self):
        self.running = True
        self.WindSpeedSum = 0
        self.WindSpeedSumCount = 0
        self.currentTime = time.time()
        while self.running:
            WindSpeed = self.calculateWindSpeed(self.readAdc())
            self.WindSpeedSum = self.WindSpeedSum + WindSpeed
            self.WindSpeedSumCount = self.WindSpeedSumCount + 1
            if(time.time()-self.currentTime > self.period):
                    self.currentTime = time.time()
                    AverageWindSpeed = self.WindSpeedSum/self.WindSpeedSumCount
                    # print(str(self.id)+" "+str(AverageWindSpeed))
                    dataToSend = [str(datetime.datetime.now()),AverageWindSpeed,self.id]
                    try:
                        self.queueForDataToSend.put_nowait(dataToSend)
                    except Exception as inst:
                        print type(inst)   
                        print inst.args
                        print inst
                        print ("Queue full "+self.id)
                        sys.stdout.flush()
                    self.WindSpeedSum = 0
                    self.WindSpeedSumCount = 0
            

    def stop(self):
        self.running = False

    def readAdc(self):
        index = 0
        values = [0]*len(self.adcPins)
        self.mutex.acquire()
        for i in self.adcPins:
            values[index] = self.adcDevice.read_adc(i, gain=GAIN)
            index = index + 1
        self.mutex.release()
        return values

    def calculateWindSpeed(self,values):
        # print(values)
        RV_Wind_Volts = values[1]*ADC_TO_VOLTS
        TMP_Therm_ADunits = values[0]*ADC_CONVERSION_FACTOR
        TempCtimes100 = (0.005 *(TMP_Therm_ADunits * TMP_Therm_ADunits)) - (16.862 * TMP_Therm_ADunits) + 9075.4

        zeroWind_ADunits = -0.0006*(TMP_Therm_ADunits * TMP_Therm_ADunits) + 1.0727 * TMP_Therm_ADunits + 47.172
        zeroWind_volts = (zeroWind_ADunits * 0.0048828125) - self.zeroWindAdjustment 
        # print zeroWind_volts
        # print zeroWind_ADunits
        try:
            WindSpeed_KMPH = pow(((RV_Wind_Volts - zeroWind_volts) /.2300) , 2.7265)*1.609
        except:
            WindSpeed_KMPH = 0
        # print WindSpeed_KMPH
        return WindSpeed_KMPH